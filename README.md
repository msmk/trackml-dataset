Support tools for the Tracking Machine Learning (TrackML) challenge
===================================================================

Contains scripts, tools, writeups, and other support tools for the TrackML
challenge. This is a private repository and only intended to be used by the
organizers.

## Dataset

A dataset comprises multiple independent events. An event is a record of the
measurements of particles from a collision of proton bunches at the Large Hadron
Collider. For each event, a certain number of particles are generated close to
the center of the detector. They propagate through the detector on curved paths
due to a magnetic field. The path they take (arc of a helix) is mostly
determined by their initial parameters, i.e. position and momentum, and the
particle charge. In each detector module that a particles passes one or more
detector cells are activated, e.g. pixels in the innermost detector layers or
strips in the outer detectors. The group of active cells on one detector module
originating from the particle is called a hit. Each hit has an estimated
position in the global coordinate system. The set of hits generated from all
particles in one collision form the basic input for the challenge. Detector
modules along the path can differ both in their granularity and whether they
measure additional information in addition to the hit position. Both the
propagation of particles and their measurement are not fully deterministic and
contain stochastic noise, e.g. small random direction and energy changes due to
interactions with the material. The measurements are not fully efficient;
particles can pass a detector without activating any cells. There are also
unassociated hits that originate from detector noise or non-reconstructible
particles.

The goal of the tracking machine learning challenge is to group the recorded
hits for each event into tracks, sets of hits that belong to the same initial
particle. A solution must uniquely associate each hit to one track (although
some hits can be left unassigned). The training dataset contains the recorded
hits, their truth association to particles, and the initial parameters of those
particles. The test dataset contains only the recorded hits.

The detailed description of the dataset format can be found in the
[public trackml repository][trackml-public] to avoid duplication here.

## Example files

Multiple examples are provided. Each example folder contains a `dataset`
folder with the generated data including the truth information. They can
also contain various `submission-...` files that contain reconstructed
solutions for the corresponding dataset.

*   `example_standard`: contains standard events as the one which will
    be used for the challenge. The events were generated using 200
    particles collisions distributed along the beam axis in the central
    region of the detector. The resulting particles were propagated in a
    2T constant magnetic field with process noise from interactions with
    the material, e.g. multiple scattering, energy loss, ... . A typical
    event contains ~14k tracks (not all will be visible if too short)
    with ~100k hits.
*   `example_standard/submission-shuffle005.csv`: solution generated by
     assigning 5% of all hits randomly to the wrong track.

## Python code and example Jupyter notebooks

A python package is available in the `trackml` directory that provides basic
functionality to load the dataset. The basic functionality requires the `pandas`
library. Some score functions also require the `scikit-learn` library. The
following code reads a single event from a dataset into three pandas
`DataFrame`s with the field names of each as defined in the dataset description
below:

    from trackml.dataset import load_event
    
    hits, cells, particles, truth = load_event('example_standard/dataset/event000000000')

This module is used in the standalone scripts in `scripts` and in the example
Jupyter notebooks (Work In Progress!) in the `notebooks` directory that show its
basic functionality.

## License

All code is licensed under MIT license.

    Copyright (c) 2017-2018 The TrackML collaborators

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

All other material, e.g. documentation and images is licensed under the Creative
Commons Attribution 4.0 International License (CC-BY-4.0). To view a copy of
this license please visit [CC-BY-4.0][cc-by-40].


[cc-by-40]: http://creativecommons.org/licenses/by/4.0/
[trackml-public]: https://github.com/LAL/trackml-public
