from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import numpy as np
import pandas as pd


def cartesian_to_cylindrical(x, y, z):

    r = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    z = z

    return r, phi, z


def create_hough_matrix(hit_id, x, y, z, r, phi, N):

    hits_dict = {'HitID': [], 'X': [], 'Y': [], 'Z': [], 'R': [], 'Phi': [], 'Theta': []}
    theta = list(np.linspace(-1 * np.pi, 1 * np.pi, N))

    for i in range(len(hit_id)):
        hits_dict['HitID'] += [hit_id[i]] * len(theta)
        hits_dict['X'] += [x[i]] * len(theta)
        hits_dict['Y'] += [y[i]] * len(theta)
        hits_dict['Z'] += [z[i]] * len(theta)
        hits_dict['R'] += [r[i]] * len(theta)
        hits_dict['Phi'] += [phi[i]] * len(theta)
        hits_dict['Theta'] += theta

    hough_matrix = pd.DataFrame(hits_dict)
    return hough_matrix


def add_r0_inv(hough_matrix):

    hough_matrix['R0Inv'] = (2. * np.cos(hough_matrix['Phi'] - hough_matrix['Theta']) / hough_matrix['R']).values

    return hough_matrix


def add_gamma(hough_matrix):
    # z_hit/(np.pi - 2*(phi_hit - theta))
    # hough_matrix['Gamma'] = ((np.pi - 2. * (hough_matrix['Phi'] - hough_matrix['Theta'])) / hough_matrix['Z']).values
    # hough_matrix['Gamma'] = np.ones(len(hough_matrix))
    hough_matrix['Gamma'] = hough_matrix['Z'] / hough_matrix['R']

    return hough_matrix


def digitize_column(hough_matrix, col, N, min_val=None, max_val=None):

    x = hough_matrix[col].values
    if min_val is not None and max_val is not None:
        bins = np.linspace(min_val, max_val, N)
    else:
        bins = np.linspace(x.min(), x.max(), N)
    bin_ids = np.digitize(x, bins)
    hough_matrix[col + 'Digi'] = bin_ids

    return hough_matrix


def cut_min_max_bins(hough_matrix, col):

    digi = hough_matrix[col].values
    hough_matrix = hough_matrix[(digi > digi.min()) * (digi < digi.max())]

    return hough_matrix


def combine_digi(hough_matrix, columns):

    hough_matrix['ComboDigi'] = np.zeros(len(hough_matrix))

    for i_col, acol in enumerate(columns):
        digi = hough_matrix[acol]
        hough_matrix['ComboDigi'] += digi * 10**(i_col * 5)

    return hough_matrix


def count_combo_digi(hough_matrix):

    unique, indeces, counts = np.unique(hough_matrix['ComboDigi'].values,
                                        return_counts=True, return_inverse=True)

    hough_matrix['ComboDigiCounts'] = counts[indeces]

    return hough_matrix


def reduce_matrix(hough_matrix, min_counts):

    return hough_matrix[hough_matrix['ComboDigiCounts'] >= min_counts].reset_index()


def sorted_groups(hough_matrix):

    gb = hough_matrix.groupby('ComboDigi')
    groups = np.array(list(gb.groups.values()))
    group_size = [len(i) for i in groups]

    groups = groups[np.argsort(group_size)[::-1]]

    return groups


#############################################################################################

def hough_transform_clusterer(x, y, z, N_bins_theta, N_bins_r0inv, N_bins_gamma, min_hits):

    hit_id = np.arange(len(x))

    r, phi, z = cartesian_to_cylindrical(x, y, z)
    hough_matrix = create_hough_matrix(hit_id, x, y, z, r, phi, N_bins_theta)

    hough_matrix = add_r0_inv(hough_matrix)
    hough_matrix = add_gamma(hough_matrix)

    hough_matrix = digitize_column(hough_matrix, 'Theta', N_bins_theta)
    hough_matrix = digitize_column(hough_matrix, 'R0Inv', N_bins_r0inv, -2. / 100, 2. / 100)
    hough_matrix = digitize_column(hough_matrix, 'Gamma', N_bins_gamma)

    hough_matrix = cut_min_max_bins(hough_matrix, 'R0InvDigi')
    # hough_matrix = cut_min_max_bins(hough_matrix, 'GammaDigi')

    hough_matrix = combine_digi(hough_matrix, ['ThetaDigi', 'R0InvDigi', 'GammaDigi'])
    # hough_matrix = combine_digi(hough_matrix, ['ThetaDigi', 'R0InvDigi'])
    hough_matrix = count_combo_digi(hough_matrix)
    hough_matrix = reduce_matrix(hough_matrix, min_hits)
    hough_matrix
    groups = sorted_groups(hough_matrix)

    track_id = 0
    track_labels = -1 * np.ones(len(hit_id))
    used = np.zeros(len(hit_id))

    matrix_hit_id = hough_matrix['HitID'].values

    for gr in groups:

        ids = matrix_hit_id[gr]
        unique_hit_ids = np.unique(ids)
        not_used_ids = unique_hit_ids[used[unique_hit_ids] == 0]

        if len(not_used_ids) >= min_hits:

            track_labels[not_used_ids] = track_id
            used[not_used_ids] += 1
            track_id += 1

    return hough_matrix, track_labels


class HoughTracker():
    def __init__(self, N_bins_theta=1000, N_bins_r0inv=200, N_bins_gamma=100, min_hits=3, verbose=1):
        super().__init__()
        self.N_bins_theta = N_bins_theta
        self.N_bins_r0inv = N_bins_r0inv
        self.N_bins_gamma = N_bins_gamma
        self.min_hits = min_hits
        self.verbose = verbose

    def fit_generator(self, event_generator):
        pass

    def predict_generator(self, event_generator):
        sub_list = [self.predict_one_event(event_id, event) for event_id, event in event_generator]
        submission = pd.concat(sub_list)
        return submission

    def predict_one_event(self, event_id, event):
        _, labels = hough_transform_clusterer(event.x.values,
                                              event.y.values,
                                              event.z.values,
                                              self.N_bins_theta,
                                              self.N_bins_r0inv,
                                              self.N_bins_gamma,
                                              self.min_hits,)
        sub = pd.DataFrame(data=np.column_stack((event.hit_id.values, labels)),
                           columns=["hit_id", "track_id"]).astype(int)
        sub['event_id'] = event_id
        return sub
