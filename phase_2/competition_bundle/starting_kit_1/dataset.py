########################################################################
# =====================  TrackML CHALLENGE DATASET  ====================
########################################################################
# Author: Isabelle Guyon, Victor Estrade
# Date: Apr 10, 2018

# ALL INFORMATION, SOFTWARE, DOCUMENTATION, AND DATA ARE PROVIDED "AS-IS".
# PARIS-SUD UNIVERSITY, THE ORGANIZERS OR CODE AUTHORS DISCLAIM
# ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY PARTICULAR PURPOSE, AND THE
# WARRANTY OF NON-INFRIGEMENT OF ANY THIRD PARTY'S INTELLECTUAL PROPERTY RIGHTS.
# IN NO EVENT SHALL PARIS-SUD UNIVERSITY AND/OR OTHER ORGANIZERS BE LIABLE FOR ANY SPECIAL,
# INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF SOFTWARE, DOCUMENTS, MATERIALS,
# PUBLICATIONS, OR INFORMATION MADE AVAILABLE FOR THE CHALLENGE.

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import bz2
import gzip
import ast
import glob

import numpy as np
import pandas as pd

"""TrackML dataset loading"""

__authors__ = ['Moritz Kiehn', 'Sabrina Amrouche']


def read_hits(filename, separated_pixels=False):
    if filename.endswith(".gz"):
        fp = gzip.open(filename, 'rt')
    elif filename.endswith(".bz2"):
        fp = bz2.open(filename, 'rt')
    else:
        fp = open(filename)

    if fp is not None:
        header = [x.strip() for x in fp.readline().split(',')]
        if not separated_pixels:
            parsed_line = []
            for line in fp:
                if line.strip():
                    pline = ast.literal_eval(line)
                    features = list(pline[:8])
                    clusters = list(pline[8:])
                    features.append( [list(clusters[i:i + 3]) for i in range(0, len(clusters), 3)] )
                    parsed_line.append(features)
            df_data = pd.DataFrame(parsed_line)
        else:
            df_data = pd.DataFrame( [ast.literal_eval(line) for line in fp if line.strip()] )
            df_data.columns = header
        fp.close()
    header[8] = "pixels"
    df_data.columns = header[:9]
    return df_data


def load_hit_generator(data_path, debug_mode=0, **kw):
    """
    Provide an iterator over all events in a dataset directory.

    For each event it returns the event_id and the output of `load_event`
    concatenated as a single tuple.
    """
    file_list = glob.glob(os.path.join(data_path, 'event*-hits*'))
    file_list = sorted(file_list)

    if debug_mode >= 3:
        print(" [d] load_hit_generator : Debug")
        print(" [d] ", "\n [d] ".join(file_list), sep='')

    name_list = set(os.path.basename(file).split('-', 1)[0] for file in file_list)
    for name, file in zip(name_list, file_list):
        event_id = int(name[5:])
        yield event_id, read_hits(file, **kw)


def load_hits(data_path, debug_mode=0):
    hit_list = []
    for event_id, hit in load_hit_generator(data_path, debug_mode=debug_mode):
        hit['event_id'] = event_id
        hit_list.append(hit)
    data = pd.concat(hit_list)
    return data


def load_validation_hit(data_path, debug_mode=0, **kw):
    file_list = glob.glob(os.path.join(data_path, 'event*-hits*'))
    i = np.random.choice(len(file_list))
    file = file_list[i]
    if debug_mode >= 3:
        print(" [d] load_validation_hit : Debug")
        print(" [d] ", i, "th file" )
        print(" [d] ", file )
    hits = read_hits(file)
    return hits
