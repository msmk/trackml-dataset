from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import pandas as pd

from sklearn.base import BaseEstimator
from joblib import Parallel, delayed


class BaseTracker(BaseEstimator):
    def __init__(self):
        super().__init__()
        self.parameters

    def predict(self, X):
        raise NotImplemented("The predict method should be implemented")
