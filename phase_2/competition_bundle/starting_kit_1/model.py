########################################################################
# ======================  TrackML CHALLENGE MODEL  =====================
########################################################################
# Author: Isabelle Guyon, Victor Estrade
# Date: Apr 10, 2018

# ALL INFORMATION, SOFTWARE, DOCUMENTATION, AND DATA ARE PROVIDED "AS-IS".
# PARIS-SUD UNIVERSITY, THE ORGANIZERS OR CODE AUTHORS DISCLAIM
# ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY PARTICULAR PURPOSE, AND THE
# WARRANTY OF NON-INFRIGEMENT OF ANY THIRD PARTY'S INTELLECTUAL PROPERTY RIGHTS.
# IN NO EVENT SHALL PARIS-SUD UNIVERSITY AND/OR OTHER ORGANIZERS BE LIABLE FOR ANY SPECIAL,
# INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF SOFTWARE, DOCUMENTS, MATERIALS,
# PUBLICATIONS, OR INFORMATION MADE AVAILABLE FOR THE CHALLENGE.

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import numpy as np
import pandas as pd

from sklearn.cluster import DBSCAN
from hough import HoughTracker


class Model():
    def __init__(self):
        self.model = HoughTracker()
        # self.model = DBSCANTracker()

    def predict_one_event(self, event_id, X):
        pred = self.model.predict_one_event(event_id, X)
        return pred


class DBSCANTracker():
    def __init__(self, eps=0.5, min_samples=3, metric='euclidean',
                 algorithm='auto', leaf_size=30, p=None, n_jobs=1, verbose=1):
        super().__init__()
        self.dbscan = DBSCAN(eps=eps, min_samples=min_samples,
                             metric=metric, algorithm=algorithm,
                             leaf_size=leaf_size, p=p, n_jobs=n_jobs)
        self.eps = eps
        self.min_samples = min_samples
        self.metric = metric
        self.algorithm = algorithm
        self.leaf_size = leaf_size
        self.p = p
        self.n_jobs = n_jobs
        self.verbose = verbose

    def fit_generator(self, event_generator):
        pass

    def predict(self, X):
        sub_list = [self.predict_one_event(event_id, hits) for event_id, hits in X.groupby('event_id')]
        submission = pd.concat(sub_list)
        return submission

    def predict_generator(self, event_generator):
        sub_list = [self.predict_one_event(event_id, event) for event_id, event in event_generator]
        submission = pd.concat(sub_list)
        return submission

    def predict_one_event(self, event_id, event):
        event = event.copy()
        event['r'] = np.hypot(event['x'], event['y'])
        event['phi'] = np.arctan2(event['y'], event['x'])
        X = event[['r', 'phi', 'z']]
#         X = event[['phi', 'z']]
#         X = event['phi'].values.reshape(-1, 1)
#         X = StandardScaler().fit_transform(X)
        labels = self.dbscan.fit_predict(X)

        sub = pd.DataFrame(data=np.column_stack((event.hit_id.values, labels)),
                           columns=["hit_id", "track_id"]).astype(int)
        sub['event_id'] = event_id
        return sub
