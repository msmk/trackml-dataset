#!/usr/bin/env python
########################################################################
# ====================  TrackML CHALLENGE INGESTION  ===================
########################################################################

# Author: Isabelle Guyon, Victor Estrade
# Date: Apr 10, 2018

# ALL INFORMATION, SOFTWARE, DOCUMENTATION, AND DATA ARE PROVIDED "AS-IS".
# PARIS-SUD UNIVERSITY, THE ORGANIZERS OR CODE AUTHORS DISCLAIM
# ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY PARTICULAR PURPOSE, AND THE
# WARRANTY OF NON-INFRIGEMENT OF ANY THIRD PARTY'S INTELLECTUAL PROPERTY RIGHTS.
# IN NO EVENT SHALL PARIS-SUD UNIVERSITY AND/OR OTHER ORGANIZERS BE LIABLE FOR ANY SPECIAL,
# INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF SOFTWARE, DOCUMENTS, MATERIALS,
# PUBLICATIONS, OR INFORMATION MADE AVAILABLE FOR THE CHALLENGE.

# Usage: python ingestion.py input_dir output_dir ingestion_program_dir submission_program_dir

# AS A PARTICIPANT, DO NOT MODIFY THIS CODE.
#
# This is the "ingestion program" written by the organizers.
# This program also runs on the challenge platform to test your code.
#
# The input directory input_dir (e.g. sample_data/) contains the dataset(s), including:
# developpement/
# final/
#
# The output directory output_dir (e.g. sample_result_submission/)
# will receive the predicted values (no subdirectories):
#   developpement_tracks.csv
#   final_tracks.csv
#
# The code directory submission_program_dir (e.g. sample_code_submission/) should contain your
# code submission model.py (and possibly other functions it depends upon).
#
# We implemented 2 functions:
# 1) load_hits:
#    ----------
#   Load all the data.
# 2) Model:
# ---------
#   Make predictions.

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import time
import os
import sys
import glob
import datetime

import numpy as np
import pandas as pd

overall_start = time.time()         # <== Mark starting time

# ==========================================================================
# =========================== BEGIN USER OPTIONS ===========================
# ==========================================================================
# Recommended to keep verbose = True: shows various progression messages
verbose = True  # outputs messages to stdout and stderr for debug purposes

# Version of the ingestion program
version = 1

# Debug level:
# 0: run the code normally, using the time budget of the tasks
# 1: run the code normally, but limits the time to max_time
# 2: run everything, but do not train, generate random outputs in max_time
# 3: stop before the loop on datasets
# 4: just list the directories and program version
debug_mode = 0

# Time budget
# Maximum time of prediction execution in seconds.
max_time = 400  # TODO adjust

# Number of events
max_events = float('Inf')

# I/O defaults
# If true, the previous output directory is not overwritten, it changes name
save_previous_results = False

# Use default location for the input and output data:
# If no arguments to run.py are provided, this is where the data will be found
# and the results written to. Change the root_dir to your local directory.
root_dir = "../"
default_input_dir = os.path.join(root_dir, "sample_data")
default_output_dir = os.path.join(root_dir, "sample_result_submission")
default_program_dir = os.path.join(root_dir, "ingestion_program")
default_submission_dir = os.path.join(root_dir, "sample_submission")

# =============================================================================
# =========================== END USER OPTIONS ================================
# =============================================================================

the_date = datetime.datetime.now().strftime("%y-%m-%d-%H-%M")

# ==========================================================================
# =========================== BEGIN PROGRAM ================================
# ==========================================================================


def parse_args():
    from argparse import ArgumentParser

    parser = ArgumentParser(description='score a solution')
    parser.add_argument('input_dir', default=default_input_dir, nargs='?')
    parser.add_argument('output_dir', default=default_output_dir, nargs='?')
    parser.add_argument('program_dir', default=default_program_dir, nargs='?')
    parser.add_argument('submission_dir', default=default_submission_dir, nargs='?')

    parser.add_argument('-v', '--verbose', default=verbose, action='store_true')
    parser.add_argument('-s', '--save', default=save_previous_results, action='store_true')
    parser.add_argument('--max-time', default=max_time, type=int)
    parser.add_argument('-d', '--debug-mode', default=debug_mode, type=int)

    args = parser.parse_args()
    return args


def mvdir(source, dest):
    ''' Move a directory'''
    if os.path.exists(source):
        os.rename(source, dest)


def test_valid_model(model, input_dir, threshold=0, debug_mode=0):
    from score import compute_score
    from dataset import load_validation

    validation_dir = os.path.join(input_dir, 'validation')
    event_id, validation_hit, validation_solution = load_validation(validation_dir, debug_mode)

    pred = model.predict_one_event(event_id, validation_hit)
    scores, n_purity_reco, n_purity_truth = compute_score(pred, validation_solution)
    score_mean = np.mean(scores)
    if debug_mode >= 3:
        print(" [d] test_valid_model : Debug")
        print(" [d] score = {}".format(score_mean))
    return score_mean > threshold


def test_submission(model, X, max_time, vprint):
    predictions = []
    execution_success = True
    start = time.time()
    vprint("************************************************")
    vprint("\n========== Ingestion program version " + str(version) + " ==========\n")
    vprint("************************************************")
    for event_id, hits in X.groupby('event_id'):  # Loop over datasets
        vprint("********** Processing event {:5d} **********".format(event_id), flush=True)
        # Keep track of time not to exceed your time budget.
        # Time spent to read data is not taken into account.
        event_start = time.time()
        pred = model.predict_one_event(event_id, hits)
        predictions.append(pred)
        event_stop = time.time()

        vprint(" [+] Time spent for this event {:5.2f} sec".format(event_stop - event_start))
        time_spent = event_stop - start
        remaining_time = max_time - time_spent
        vprint(" [+] Remaining time for this task {:5.2f} sec".format(remaining_time))

        if time_spent >= max_time:
            vprint(" [-] Sorry, time budget exceeded, skipping the remaining events")
            execution_success = False
            break

    return predictions, execution_success, start


# ==========================================================================
# ============================ MAIN PROGRAM ================================
# ==========================================================================
def main():
    args = parse_args()

    if args.verbose:
        def vprint(value, *args, sep=' ', end='\n', file=sys.stdout, flush=False):
            print(value, *args, sep=sep, end=end, file=file, flush=flush)
    else:
        def vprint(value, *args, sep=' ', end='\n', file=sys.stdout, flush=False):
            pass

    vprint(vars(args))

    input_dir = args.input_dir
    output_dir = args.output_dir
    program_dir = args.program_dir
    submission_dir = args.submission_dir
    debug_mode = args.debug_mode
    max_time = args.max_time

    vprint("Using input_dir: ", input_dir)
    vprint("Using output_dir: ", output_dir)
    vprint("Using program_dir: ", program_dir)
    vprint("Using submission_dir: ", submission_dir)

    # Check whether everything went well (no time exceeded)
    execution_success = True

    # Our libraries
    sys.path.append(program_dir)
    sys.path.append(submission_dir)
    from dataset import load_hits
    from model import Model

    if debug_mode >= 4:  # Show library version and directory structure
        print( "\n".join(glob.glob(".")) )
        print( "\n".join(glob.glob("./*")) )
        print( "\n".join(glob.glob("./*/*")) )
        return  # Stop the program here

    # Move old results and create a new output directory (useful if you run locally)
    if save_previous_results and os.listdir(output_dir):
        old_output_dir = output_dir + '_' + the_date
        vprint('Moving old results to :', old_output_dir)
        mvdir(output_dir, old_output_dir)
    os.makedirs(output_dir, exist_ok=True)

    # Load model
    vprint('Loading the model')
    model = Model()

    # Test valid model
    vprint('Loading the validation event')
    is_valid = test_valid_model(model, input_dir, threshold=0, debug_mode=debug_mode)

    predictions = None
    if is_valid:
        # Load data
        vprint('Loading the data')
        X = load_hits(input_dir, debug_mode)
        predictions, execution_success, start = test_submission(model, X, max_time, vprint)
    else:
        vprint(" [-] Not valid model !")
        start = time.time()
        execution_success = False

    # Concat track
    if predictions:
        predictions = pd.concat(predictions)
    else:
        predictions = pd.DataFrame({"event_id": [0], "hit_id": [0], "track_id": [0]})

    # Write results
    submission_path = os.path.join(output_dir, 'submission.csv')
    predictions.to_csv(submission_path, index=False, columns=["event_id", "hit_id", "track_id"])

    time_spent = time.time() - start
    vprint(" [+] Results saved, time spent so far {:5.2f} sec".format(time_spent))
    with open(os.path.join(output_dir, 'time.txt'), 'w') as f:
        print(time_spent, file=f)

    if execution_success:
        vprint(" [+] Done")
    else:
        vprint(" [-] Done, but some events aborted because time limit exceeded")


if __name__ == "__main__":
    main()
