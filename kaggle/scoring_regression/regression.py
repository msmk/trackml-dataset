#!/usr/bin/env python
"""
perform multiple regression test of TrackML/scoring/score.py
TODO : check ill formed submission
"""
import sys
sys.path.insert(0,"../../")
#from scoring.score_slow import score_file
from scoring.score import score_file
fsolution="solution"

rtests=[
	[fsolution,"submission-ideal",1.],
	[fsolution,"submission-noise",1.], # adding noise hits do ot change the score (unless track killed)
	[fsolution,"submission-missing",0.732245], # it is valid to not list some hits (with impact on the score)
	[fsolution,"submission-only2tracks",0.76944], # only 2 tracks in total
	[fsolution,"submission-short3",0.76944], # track with 3 hit or less are killed (=only2)
	[fsolution,"submission-short4",0.89444], # track with 4 hit or more are kept
	[fsolution,"submission-only1event",0.5], # no track found in first event, score should still be averaged on two
	[fsolution,"submission-purity-reco",0.5], # <51% of hit (noise included) from same track=>kill track==>only1
	[fsolution,"submission-purity-truth",0.5], # <51% of hit of the best true track=>kill track=>only1
	[fsolution,"submission-shuffle",1.], # shuffling should not impact score (although arguably shuffling between events is extreme)
	["../../example_standard/solution","../../example_standard/submission-ideal",1.], 
	["../../example_standard/solution","../../example_standard/submission-shuffle005",0.950710], 
	]


overall=True
successes=[]
for rtest in rtests:
	fsolution=rtest[0]+".csv"
	fsubmission=rtest[1]+".csv"
	score_expected=rtest[2]
	print ("--------------regression test : ",fsubmission," -------------------")
	score=score_file(fsolution,fsubmission)
	if abs(score-score_expected)>1E-5:
		print (fsubmission," ERROR score={}".format(score)," while expected={}".format(score_expected))
		success=False
		overall=False
	else:
		print (fsubmission, "SUCCESS")	
		success=True
	successes+=[success]

print ("\n \n --------------------------------")

for i in range(len(rtests)):
	print ("test ",i," ",rtests[i][0]," ",rtests[i][1]," ",successes[i])	

print ("--------------------------------")
if overall: 
	print ("OVERALL SUCCESS!") 
else: 
	print ("OVERALL FAILURE!")