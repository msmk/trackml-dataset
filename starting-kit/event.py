import numpy as np
import pandas as pd
from scipy import interpolate




def check_submission(y):
    good = False
    return good


def reset_ids(y):
    # (1- Run on truth) To simplify the particle ids,
    # from very large integers to 0,1,2,3...
    # (2- Run on solution) If there are unclustered single hits
    # make sure they are all assigned to -1
    # may leave gaps in the final result: -1,0,1,4,6 etc...
    # it's ok, actual value of any ids don't matter
    initial_ids = np.unique(y[y != -1])
    y_new = np.array([])
    for value in y:
        value_new = -1
        if(value >= 0):
            # keep all single hit tracks as unassigned (-1)
            nhits = len(np.where(y == value)[0])
            if(nhits > 1):
                value_new = np.where(initial_ids == value)[0]
        y_new = np.append(y_new, value_new)
    return y_new


def get_row(data, id):
    # Selects flattened row out of any table
    # particles, hits, etc...
    row = data[
        np.where(data[:, 0] == id)[0],
        :].flatten()
    return row


class event:



    def __init__(self, event_id=0,
                 is_truth=True, is_solved=True, load_weights=False,
                 path2hits="../example/",path2weights="../example/",
                 path2solutions="../example/",path2output="./"):
        # self.hits = read_file(event_id, "hits")
        self.path2hits=path2hits # input path for hits and truth
        self.path2weights=path2weights # input path for weights
        self.path2solutions=path2solutions # input path for solutions
        self.path2output=path2output # input path for output (whatever it is)
        if(is_truth):
            self.particles = self.read_file(event_id, "particles",self.path2hits)
            self.truth = self.read_file(event_id, "truth",self.path2hits)
            self.pts = np.zeros(len(self.particles))
            self.weights = np.array([])
        if(is_solved):
            self.solution = self.read_file(event_id, "solution",self.path2solutions)
        if(load_weights):
            self.weights = self.read_file(event_id, "weights",self.path2weights)


    def read_file(self,event_id, file_type, path2):
        filename = path2+"/event"
        filename += str(event_id).zfill(9)
        filename += "-" + file_type + ".csv"
        print("Reading",filename)
        data = pd.read_csv(filename)
        y = data.values
        print("...read",len(y)," lines")
        return y


    def calculate_weights(self):
        w_proposal = [10., 8., 6., 5., 3., 3., 3., 5., 6.]
        x = np.arange(0, len(w_proposal)) / (len(w_proposal) - 1.)
        self.weight_function = interpolate.interp1d(x, w_proposal)

        for hit_id, hit in enumerate(self.truth):
            pt = self.get_pt(hit_id)
            weight = self.weight_pt(pt) * self.weight_order(hit_id)
            self.weights = np.append(self.weights, weight)

    def get_pt(self, hit_id):
        pt = 0.
        particle_id = self.truth[hit_id, 1]
        if(particle_id >= 0):
            particle = get_row(self.particles, particle_id)
            px = particle[4]
            py = particle[5]
            pt = np.sqrt(px**2 + py**2)
        return pt

    def weight_pt(self, pt, pt_inf=0.5, pt_sup=5, w_min=0.1, w_max=1.):
        weight = 0.
        if(pt > 0):
            weight = w_min #DR actually should set very low pt to zero
            if(pt > pt_inf):
                weight += (w_max - w_min) * (pt - pt_inf) / (pt_sup - pt_inf)
                if(pt > pt_sup):
                    weight = w_max
        return weight

    def weight_order(self, hit_id, normalize_by_hits=True):
        particle_id = self.truth[hit_id, 1]
        n_hits = np.sum(self.truth[:, 1] == particle_id)
        #        hit = get_row(self.hits, hit_id)
        #        order = hit[0]  # ?
        # Temporary substitution
        order = np.random.randint(0, n_hits)
        weight = self.weight_function(order / n_hits)
        if(normalize_by_hits):
            norm = 0.
            for i in np.arange(0, n_hits):
                norm += self.weight_function(i / n_hits)
            weight /= norm
        return weight
