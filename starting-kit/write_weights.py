import numpy as np
import pandas as pd
from event import event
import optparse


def write_weights(path2hits="../example/",path2output="./",ifevent=0,nevents=2):
    #    ifevent : first event index
    #    nevents : number of events
    events = range(ifevent,ifevent+nevents)
    for event_id in events:
        event_data = event(event_id,load_weights=False,is_solved=False,
                           path2hits=path2hits,path2output=path2output)
        event_data.calculate_weights()
        weights = event_data.weights
        output = pd.DataFrame({'weight': weights},
                              dtype=float)
        filename = path2output+"/event"
        filename += str(event_id).zfill(9)
        filename += "-weights.csv"
        print("Writing",filename)
        output.to_csv(filename, index=False)
        print("...wrote",output.shape[0]," lines")


if __name__ == "__main__":
    parser = optparse.OptionParser()

    parser.add_option('-p', '--path2hits',
                      help="path to event data", default="./")

    parser.add_option('-f', '--firstevent',
                      help="first of event to consider", default=0)

    parser.add_option('-n', '--nevents',
                      help="number of events to consider", default=5)

    parser.add_option('-o', '--output',
                      help="path to output file", default="")

    parser.add_option('-v','--verbose',dest="verbose",help="enable verbose event by event printout",action='store_true')

    options, args = parser.parse_args()


    #path to event data, change accordingly
    path2hits= options.path2hits
    #Number of event files to load
    nevents= int(options.nevents)
    ifevent= int(options.firstevent)
    path2weights=options.output

    write_weights(path2hits=path2hits,path2output=path2weights,ifevent=0,nevents=nevents)
