import numpy as np
from event import event, reset_ids
import optparse



def score_event(y_true, y_pred, weights, penalize_bad_hits=False):
    '''Compute a clustering score.
    Cluster ids should be nonnegative integers. A negative integer
    will mean that the corresponding point does not belong to any
    cluster.
    We first identify assigned clusters by taking the max count of
    unique assigned ids for each true cluster. We remove all unassigned
    clusters (all assigned ids are -1) and all duplicates (the same
    assigned id has majority in several true clusters) except the one
    with the largest count. We add the counts, then divide by the number
    of events. The score should be between 0 and 1.
    Parameters
    ----------
    y_true : np.array, shape = n
        The ground truth.
        column: cluster_id
    y_pred : np.array, shape = n
        The predicted cluster assignment (predicted cluster_id)
    """
    '''

    score = 0.
    penalties = weights
    cluster_ids_true = reset_ids(y_true)
    cluster_ids_pred = reset_ids(y_pred)

    unique_cluster_ids = np.unique(cluster_ids_true)
    n_cluster = len(unique_cluster_ids)

    # assigned_clusters[i] will be the predicted cluster id
    # we assign (by majority) to true cluster i
    assigned_clusters = np.empty(n_cluster, dtype='int64')
    # true_positives[i] will be the number of points in
    # predicted cluster[assigned_clusters[i]]
    true_positives = np.full(n_cluster, fill_value=0, dtype='float')
    false_positives = np.full(n_cluster, fill_value=0, dtype='float')

    for i, cluster_id in enumerate(unique_cluster_ids):
        # predicted points belonging to a cluster
        found_points = cluster_ids_pred[cluster_ids_true == cluster_id]
        weights_found_points = weights[cluster_ids_true == cluster_id]
        penalties_found_points = penalties[cluster_ids_true == cluster_id]

        # nonnegative cluster_ids (negative ones are unassigned)
        assigned_points = found_points[found_points >= 0]
        # the unique nonnegative predicted cluster ids on
        # true_cluster[i]
        n_sub_cluster = len(np.unique(assigned_points))
        # We find the largest predicted cluster in the true cluster.
        if(n_sub_cluster > 0):
            # sizes of predicted assigned cluster in true cluster[i]
            predicted_cluster_sizes = np.bincount(
                assigned_points.astype(dtype='int64'))
            # If there are ties, we assign the true cluster to the
            # predicted cluster with the smallest id (combined behavior
            # of np.unique which sorts the ids and np.argmax which
            # returns the first occurence of a tie).
            assigned_clusters[i] = np.argmax(predicted_cluster_sizes)

            # main modification from the un-weighted version
            # used in CTD Ramp
            # true_positives[i] = len(
            #    found_points[found_points == assigned_clusters[i]])
            # counting entries replaced with weighted sum:
            true_positives[i] = sum(
                weights_found_points[found_points == assigned_clusters[i]])

            false_positives[i] = sum(
                penalties_found_points[found_points == assigned_clusters[i]])

        # If none of the assigned ids are positive, the cluster is
        # unassigned and true_positive = 0
        else:
            assigned_clusters[i] = -1
            true_positives[i] = 0

    # resolve duplicates and count good assignments
    sorted = np.argsort(true_positives)
    true_positives_sorted = true_positives[sorted]
    false_positives_sorted = false_positives[sorted]
    assigned_clusters_sorted = assigned_clusters[sorted]
    good_clusters = assigned_clusters_sorted >= 0
    for i in range(len(assigned_clusters_sorted) - 1):
        assigned_cluster = assigned_clusters_sorted[i]
        # duplicates: only keep the last count (which is the largest
        # because of sorting)
        if assigned_cluster in assigned_clusters_sorted[i + 1:]:
            good_clusters[i] = False

    n_sample = np.sum(weights)
    n_good = np.sum(true_positives_sorted[good_clusters])
    n_bad = np.sum(false_positives_sorted[good_clusters])
    if(penalize_bad_hits):
        n_good -= n_bad
    score += 1. * n_good / n_sample
    return score


def event_weight(y_true, normalize_by_multiplicity=False):
    weight = 1.
    if(normalize_by_multiplicity):
        weight = len(np.unique(y_true))
    return weight


def score_sample(load_weights=True,path2hits="../example/",path2weights="../example/",
                 path2solutions="../example/",ifevent=0,nevents=2):
    #    N = 1
    #    events = range(0, N)
    events = range(ifevent,ifevent+nevents)

    score = 0.
    total_weight = 0.
    for event_id in events:
        event_data = event(event_id, load_weights=True,path2hits=path2hits,path2weights=path2weights,
                           path2solutions=path2solutions)
#        event_data.calculate_weights()
        y_true = event_data.truth[:, 1]
        y_pred = event_data.solution[:, 1]
        weights = event_data.weights
#        y_true = read_hits(event, "truth")
#        y_pred = read_hits(event, "solution")
        e_w = event_weight(y_true)
        e_s = score_event(y_true, y_pred, weights)
        score += e_w * e_s
        print("event score : ", e_s)
        total_weight += e_w

    score /= total_weight
    print("total score",score)
    return score

if __name__ == "__main__":
    parser = optparse.OptionParser()
    
    parser.add_option('--path2hits',
    help="path to event data", default="./")

    parser.add_option('--path2weights',
                  help="path to event data", default="./")

    parser.add_option('--path2solutions',
                  help="path to event data", default="./")

    parser.add_option('-f', '--firstevent',
                    help="first of event to consider", default=0)

    parser.add_option('-n', '--nevents',
                    help="number of events to consider", default=5)


    options, args = parser.parse_args()


    #path to event data, change accordingly
    path2hits= options.path2hits
    path2weights=options.path2weights
    path2solutions=options.path2solutions
    #Number of event files to load
    nevents= int(options.nevents)
    ifevent= int(options.firstevent)

    score_sample(load_weights=True,path2hits=path2hits,path2weights=path2weights,
                 path2solutions=path2solutions,ifevent=ifevent,nevents=nevents)

