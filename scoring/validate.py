"""
validate solution
"""

import numpy as np
import csv
import optparse
import pandas as pd
import glob
import os.path





def validate (path2solutions="./",path2hits="./",firstevent=0,nevent=5,verbose=False):

    debug=False
    
    
    valid=False
    
    wildname_tracks=os.path.join(path2solutions,'*-tracks.csv')

    n_sol_max=int(len(glob.glob(wildname_tracks)))
    if n_sol_max <=0:
        raise Exception ("no solution file matching ",wildname_tracks)

    if nevent==0: nevent=n_sol_max

    print("Will process",nevent," files matching ",wildname_tracks)

    wildname_hits=os.path.join(path2hits,'*-truth.csv')
    n_hits_max=int(len(glob.glob(wildname_hits)))
    
    if n_hits_max <=0:
        raise Exception ("no hits file matching ",wildname_hits)

    if n_hits_max<nevent:
        raise Exception ("not enough hits files in ",wildname_hits)

    print("hits file name matching ",wildname_hits)
    
    for i in range(firstevent,nevent):
        valid_event=False
        #path to data
        idFile="%09d"%(i,)

        fname_hits=os.path.join(path2hits,"event"+str(idFile)+"-hits.csv")
        fname_tracks=os.path.join(path2solutions,"event"+str(idFile)+"-tracks.csv")
        
        if verbose:
            print("Reading event",i," solution in ",fname_tracks," hits in ",fname_hits)

        # only care about first column
        #FIXME should use dataset reading

        hits=pd.read_csv(fname_hits,usecols=['hit_id'])
        if hits.isnull().values.any():
            raise Exception ("NaN detected. Could not read properly",fname_hits)


        tracks=pd.read_csv(fname_tracks)
        if tracks.isnull().values.any():
            raise Exception ("NaN detected. Could not read properly",fname_tracks)


        labels=tracks.columns.values
        if len(labels)<2 or labels[0]!='hit_id' or labels[1]!='track_id':
            raise Exception ("Solution file first 2 column header should be hit_id,track_id")

        if verbose:
            print("Read event",i," solution n hits associated",tracks.shape[0]," out of ",hits.shape[0])


        valid_event= validate_event(tracks,hits,verbose=verbose)

        if verbose:
            print("Examined event",i," Validity:",valid_event)

        if not valid_event:
            print ("Event",i," is invalid!")
            valid=False
                
    

    return valid

def validate_event(tracks,hits,verbose=False):
    # there should be no duplicate hit_id in tracks
    tracks_ids=tracks['hit_id'].unique()
    hits_ids=hits['hit_id'].unique()
    if len(tracks_ids)!=tracks.shape[0]:
        print ("Invalid! there are duplicated hit_id in the solution file")
        valid_event=False
        return False

    hits_ids=hits['hit_id'].unique()
    #all hit_id in tracks should exist in hits
    print(hits_ids)
    inter=np.intersect1d(hits_ids,tracks_ids)
    if len(inter)<len(tracks_ids):
        print ("Invalid! some hit_id in solution file are not in hits file!")
        print("n tracks=",len(tracks_ids), "n intersection = ",len(inter))
        #print ("missing ",[i for i in tracks_ids if i not in hits_ids])
        valid_event=False
        return False
               
               
    return True

if __name__ == "__main__":
    import optparse


    parser = optparse.OptionParser()

    parser.add_option('-s', '--path2solutions',
                  help="path to solution files",type="string", default="./")

    parser.add_option('-t', '--path2hits',
                      help="path to hits file",type="string", default="./")

    parser.add_option('-f', '--firstevent',
                      help="int of first event in range",type="int", default=0)

    parser.add_option('-n', '--nevent',
                      help="number of events to consider",type="int", default=5)


    parser.add_option('-v','--verbose',dest="verbose",help="enable verbose event by event printout",action='store_true')

    options, args = parser.parse_args()



    #Path to solution files
    path2solutions=options.path2solutions

    #path to hit filt
    path2hits= options.path2hits


    firstevent=options.firstevent
    #Number of event files to load
    nevent= int(options.nevent)

    verbose=options.verbose
    
    valid_overall=validate(path2solutions=path2solutions,path2hits=path2hits,
                                firstevent=firstevent,nevent=nevent,verbose=verbose)

