#!/usr/bin/env python
"""
scores a dataset using the TrackML custom metric
super optimised (score_slow give same results but 100 times slower)
"""

from __future__ import print_function

__authors__ = ['Sabrina Amrouche', 'David Rousseau', 'Moritz Kiehn','Ilija Vukotic']

import numpy as np
import pandas as pd
import time

def score_file(solution_file, submission_file):
    
    scores = []

    solution_dtypes = {
        'event_id': 'i4',
        'hit_id': 'i4',
        'particle_id': 'i8',
        'weight': 'f4' }
    print ("reading",solution_file)
    solution = pd.read_csv(solution_file, header=0, dtype=solution_dtypes)
    # print (solution.shape,'\n',solution.head())
    # consistency check of solution file
    print ("checking solution")

    check_solution(solution)
 
 
    submission_dtypes = {
        'event_id': 'i4',
        'hit_id': 'i4',
        'track_id': 'i4' }
    print ("reading",submission_file)

    submission = pd.read_csv(submission_file, header=0, dtype=submission_dtypes)
    # print(submission.shape,'\n',submission.head())
   #consistency check of submisison file
    print ("checking submission ")

    check_submission(submission)

    s = time.process_time()

    sol_by_ev = solution.groupby('event_id', sort=False, group_keys=False)
    # print("solution events:", len(sol_by_ev))

    sub_by_ev = submission.groupby('event_id', sort=False, group_keys=False)
    # print("submission events:", len(sub_by_ev))


 
    for event_id in sol_by_ev.groups.keys():
        print('processing event:', event_id)
        if event_id in sub_by_ev.groups.keys():
            sol_df = sol_by_ev.get_group(event_id)
            sub_df = sub_by_ev.get_group(event_id)
            scores.append(score_event(sol_df.drop('event_id', axis=1), sub_df.drop('event_id', axis=1)))
        else:
            scores.append(0.) # event missing in submission has score zero   
    e = time.process_time() - s
    print('time elapsed:', e)

    score=np.mean(scores)    

    # normalize to the number of events
    print('score mean:   {:f}'.format(score))
    print('score median: {:f}'.format(np.median(scores)))
    print('score stddev: {:f}'.format(np.std(scores)))
    return score


def score_event(solution, submission):
    
    # calculate number of hits for each particle_id
    part_hits = solution['particle_id'].value_counts(sort=False)
    
    # for scaling at the end
    tot_weight = solution['weight'].sum()
    
    event = pd.merge(submission, solution, on=['hit_id'], how='left', validate='one_to_one')

    event = event.drop('hit_id', axis=1).sort_values(['track_id','particle_id'])
    
    # print(event.shape,'\n',event.head())

    score_event = 0

    # running sum for the track we are currently in
    c_track_id = -1
    c_hits_total = -1000
    
    # running sum for the particle we are currently in
    c_particle_id = -1
    c_particle_hits = -1
    c_weight = 0

    # particle with most hits (in this track_id) up to now
    max_particle_id = -1
    max_particle_hits = -1
    max_particle_weight = 0

    for r in event.itertuples(index=False):
        tid = r[0] # track_id
        pid = r[1] # particle_id
        weight = r[2]
#       print(">>> read >>>> ", tid, pid, weight)

        if tid > c_track_id:
#           print('new track. lets get done with the old one.')
            # could be that the particle is the best one
            if c_particle_hits > max_particle_hits:
                max_particle_hits = c_particle_hits
                max_particle_id = c_particle_id
                max_particle_weight = c_weight
            
#           print ('best particle was:', max_particle_id, ' it had:', max_particle_hits, 'from total of:', c_hits_total)
            # check if best particle had more than 51% hits
            purity_reco = max_particle_hits / c_hits_total
            if purity_reco >= 0.51:
#               print("* pass purity test:", purity_reco)
                
                # check if that particle_id used up more than 51% hits
#               print( "had total possible hits:", part_hits[max_particle_id])
                purity_truth = max_particle_hits / part_hits[max_particle_id]
                if purity_truth >= 0.51:
#                   print("** pass truth purity test:", purity_truth, 'weight:', max_particle_weight)
                    # sum up weight
                    score_event += max_particle_weight

#           print("reset things for the new track")
            c_track_id=tid
            c_hits_total = max_particle_hits = 1
            c_particle_id = max_particle_id = pid
            c_particle_hits = 1
            c_weight = max_particle_weight = weight
            continue
            
#       print('old track')
        c_hits_total += 1
        if pid > c_particle_id:
#           print("    new particle")
            
            # check if last (current) particle is better than max one
            # if yes, memorize the last (current) and set variables for the new one.
            if c_particle_hits > max_particle_hits:
                max_particle_hits = c_particle_hits
                max_particle_id = c_particle_id
                max_particle_weight = c_weight

            c_particle_id = pid
            c_particle_hits = 1
            c_weight = weight
        else:     
            c_weight += weight
            c_particle_hits += 1
#           print ('    old particle - updated values - hits: ', c_particle_hits,'weight:', c_weight)
    
    # to handle last track
    if c_particle_hits > max_particle_hits:
        max_particle_hits = c_particle_hits
        max_particle_id = c_particle_id
        max_particle_weight = c_weight
    purity_reco = max_particle_hits / c_hits_total
    if purity_reco >= 0.51:
        purity_truth = max_particle_hits / part_hits[max_particle_id]
        if purity_truth >= 0.51:
            score_event += max_particle_weight


    score_event /= tot_weight
    # print("event score:", score_event)

    return score_event

def check_submission(submission):
    """
    Check the submission file is consistent
    """
    keys=submission.keys()
    if len(keys)!=3:
        raise Exception ("submission file should have exactly 3 columns. Reading {}".format(len(keys)) )
                         
    if keys[0]!="event_id":
        raise Exception ("submission file first column should be \"event_id\". Reading \"{}\" ".format(keys[0]) )         
    if keys[1]!="hit_id":
        raise Exception ("submission file second column should be \"hit_id\". Reading \"{}\"".format(keys[1]) )         
    if keys[2]!="track_id":
        raise Exception ("submission file third column should be \"track_id\". Reading \"{}\" ".format(keys[2]) )        

    #FIXME all id >=0
    
    return True

def check_solution(solution):
    """
    Check the solution file is consistent
    """
    keys=solution.keys()
    if len(keys)!=4:
        raise Exception ("solution file should have exactly 4 columns. Reading {}".format(len(keys)) )
                         
    if keys[0]!="event_id":
        raise Exception ("solution file first column should be \"event_id\". Reading \"{}\" ".format(keys[0]) )         
    if keys[1]!="hit_id":
        raise Exception ("solution file second column should be \"hit_id\". Reading \"{}\"".format(keys[1]) )         
    if keys[2]!="particle_id":
        raise Exception ("solution file third column should be \"particle_id\". Reading \"{}\" ".format(keys[2]) )        
    if keys[3]!="weight":
        raise Exception ("solution file fourth column should be \"weight\". Reading \"{}\" ".format(keys[2]) )        

    #FIXME all id >=0
    
    return True



def check_event(tracks, truth):
    """
    Check that the event and solution are consistent
    """
    # check for duplicates
    dup_mask = tracks.duplicated('hit_id')
    if np.any(dup_mask):
        dup = sorted(truth[dup_mask]['hit_id'])
        raise Exception('Tracks contain duplicated hits: {}'.format(dup))
    dup_mask = truth.duplicated('hit_id')
    if np.any(dup_mask):
        dup = sorted(truth[dup_mask]['hit_id'])
        raise Exception('Truth contains duplicated hits: {}'.format(dup))


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(description='score a submission')
    parser.add_argument('solution_file')
    parser.add_argument('submission_file')
    args = parser.parse_args()

    score_file(**vars(args))
