#!/usr/bin/env python
"""
scores a dataset using the TrackML custom metric
very slow, score.py is 100 times faster
this version is kept for reference being more readable
"""

from __future__ import print_function

__authors__ = ['Sabrina Amrouche', 'David Rousseau', 'Moritz Kiehn']

import numpy as np
import pandas as pd
import time

def score_file(solution_file, submission_file):

    solution_dtypes = {
        'event_id': 'i4',
        'hit_id': 'i4',
        'particle_id': 'i8',
        'weight': 'f4' }
    solution = pd.read_csv(solution_file, header=0, dtype=solution_dtypes)

    # consistency check of solution file
    check_solution(solution)
    
    submission_dtypes = {
        'event_id': 'i4',
        'hit_id': 'i4',
        'track_id': 'i4' }
    submission = pd.read_csv(submission_file, header=0, dtype=submission_dtypes)
    #consistency check of submisison file
    check_submission(submission)

    s = time.process_time()

    
    # combine to get the true and reconstructed particle id for each hit
    # validate checks that each key exist once and there are no duplicates
    dataset = pd.merge(solution, submission,
        on=['event_id', 'hit_id'], how='left', validate='one_to_one')

    score=score_dataset(dataset)

    e = time.process_time() - s
    print('time elapsed:', e)

    return score

def score_dataset(dataset, verbose=False):

    scores = []

    # split dataset into events
    for event_id, event in dataset.groupby('event_id', sort=False):
        print('processing event {}'.format(event_id))

        # split hits into reconstructed tracks
        score_event = 0
        for track_id, track in event.groupby('track_id', sort=False):

            # find contributing particles sorted by occurence
            particles = track['particle_id'].value_counts()
            # the particles contributing the most hits
            major_id = particles.index[0]
            major_nhits_matched = particles.iloc[0]

            # need at least half (wrt to the reco track) of hits matched
            purity_reco = major_nhits_matched / len(track)
            if purity_reco < 0.51:
                continue

            # need at least half  (wrt to the reco track) of hits matched
            # count number of true hits of the major
            major_nhits = len(event.loc[event['particle_id'] == major_id])
            purity_truth = major_nhits_matched / major_nhits
            if purity_truth < 0.51:
                continue

            # track score is the sum of the reco hit weights that belong to the major particle
            # this is the intersection between the reco and the truth
            score_track = track['weight'].where(track['particle_id'] == major_id).sum()
            score_event += score_track

        # normalize wrt total weight. A perfect reco algorithm should get score 1.
        score_event /= event['weight'].sum()
        scores.append(score_event)

    score=np.mean(scores)    
    # normalize to the number of events
    print('score mean:   {:f}'.format(score))
    print('score median: {:f}'.format(np.median(scores)))
    print('score stddev: {:f}'.format(np.std(scores)))
    return score

def check_submission(submission):
    """
    Check the submission file is consistent
    """
    keys=submission.keys()
    if len(keys)!=3:
        raise Exception ("submission file should have exactly 3 columns. Reading {}".format(len(keys)) )
                         
    if keys[0]!="event_id":
        raise Exception ("submission file first column should be \"event_id\". Reading \"{}\" ".format(keys[0]) )         
    if keys[1]!="hit_id":
        raise Exception ("submission file second column should be \"hit_id\". Reading \"{}\"".format(keys[1]) )         
    if keys[2]!="track_id":
        raise Exception ("submission file third column should be \"track_id\". Reading \"{}\" ".format(keys[2]) )        

    #FIXME all id >=0
    
    return True

def check_solution(solution):
    """
    Check the solution file is consistent
    """
    keys=solution.keys()
    if len(keys)!=4:
        raise Exception ("solution file should have exactly 4 columns. Reading {}".format(len(keys)) )
                         
    if keys[0]!="event_id":
        raise Exception ("solution file first column should be \"event_id\". Reading \"{}\" ".format(keys[0]) )         
    if keys[1]!="hit_id":
        raise Exception ("solution file second column should be \"hit_id\". Reading \"{}\"".format(keys[1]) )         
    if keys[2]!="particle_id":
        raise Exception ("solution file third column should be \"particle_id\". Reading \"{}\" ".format(keys[2]) )        
    if keys[3]!="weight":
        raise Exception ("solution file fourth column should be \"weight\". Reading \"{}\" ".format(keys[2]) )        

    #FIXME all id >=0
    
    return True



def check_event(tracks, truth):
    """
    Check that the event and solution are consistent
    """
    # check for duplicates
    dup_mask = tracks.duplicated('hit_id')
    if np.any(dup_mask):
        dup = sorted(truth[dup_mask]['hit_id'])
        raise Exception('Tracks contain duplicated hits: {}'.format(dup))
    dup_mask = truth.duplicated('hit_id')
    if np.any(dup_mask):
        dup = sorted(truth[dup_mask]['hit_id'])
        raise Exception('Truth contains duplicated hits: {}'.format(dup))

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(description='score a submission')
    parser.add_argument('solution_file')
    parser.add_argument('submission_file')
    args = parser.parse_args()

    score_file(**vars(args))
