#!/usr/bin/env python
#
# show summary statistics for a dataset
#
#    summarize_dataset.py <path/to/dataset>
#

from __future__ import print_function

import numpy as np
from trackml.dataset import load_dataset

def main(path):
    n_hits = []
    n_cells = []
    n_particles = []

    for event_id, hits, cells, particles, truth in load_dataset(path):
        print('processing event {}'.format(event_id))

        hits = hits.set_index('hit_id')
        particles = particles.set_index('particle_id')
        truth = truth.set_index(['hit_id', 'particle_id'])

        check_duplicates(hits)
        check_duplicates(particles)
        check_duplicates(truth)

        n_hits.append(len(hits))
        n_cells.append(len(cells))
        n_particles.append(len(particles))

    print('events:              {:d}'.format(len(n_hits)))
    print('hits total:          {:d}'.format(np.sum(n_hits)))
    print('hits per event:      {:g}'.format(np.mean(n_hits)))
    print('cells total:         {:d}'.format(np.sum(n_cells)))
    print('cells per event:     {:g}'.format(np.mean(n_cells)))
    print('cells per hit:       {:g}'.format(np.sum(n_cells) / np.sum(n_hits)))
    print('particles total:     {:d}'.format(np.sum(n_particles)))
    print('particles per event: {:g}'.format(np.mean(n_particles)))

def check_duplicates(df):
    """Check for duplicates in a dataframe.
    """
    if df.index.has_duplicates:
        print('duplicates:')
        print(df.loc[df.index.get_duplicates()].to_string())

if __name__ == '__main__':
    import sys
    main(*sys.argv[1:])
