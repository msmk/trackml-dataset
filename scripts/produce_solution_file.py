#!/usr/bin/env python
#
# produce the combined solution file for a dataset
#
#     produce_solution_file.py <path/to/dataset> <path/to/solution_file>
#

from __future__ import print_function

import pandas as pd
from trackml.dataset import load_dataset

def main(dataset_path, solution_file):
    first_event = True
    for event_id, truth in load_dataset(dataset_path, parts=['truth']):
        print('processing event {}'.format(event_id))

        truth['event_id'] = event_id
        kw = {
            # for order
            'columns': ['event_id', 'hit_id', 'particle_id', 'weight'],
            'index': False,
            'float_format': '%6g',
        }
        if first_event:
            truth.to_csv(solution_file, mode='w', **kw)
            first_event = False
        else:
            truth.to_csv(solution_file, mode='a', header=False, **kw)
    print('wrote \'{}\''.format(solution_file))

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(description='produce dataset solution file (to be used to score submission)')
    parser.add_argument('dataset_path')
    parser.add_argument('solution_file')
    args = parser.parse_args()

    main(**vars(args))
