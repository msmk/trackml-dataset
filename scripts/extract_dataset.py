#!/usr/bin/env python3

import pathlib

import numpy as np
import pandas as pd
from numpy.random import RandomState
from trackml.dataset import load_dataset

def main(dataset_path, extract_path, random_seed, nparticles, pt_min, nevents=None):

    extract_path.mkdir(parents=True, exist_ok=True)
    rng = RandomState(seed=random_seed)

    for event_id, *event in load_dataset(dataset_path):
        print('processing event {}'.format(event_id))

        subset = extract_nparticles(*event, nparticles=nparticles, pt_min=pt_min, rng=rng)
        names = ['hits', 'cells', 'particles', 'truth']
        for name, part in zip(names, subset):
            print('  entries {}: {:d}'.format(name, len(part)))
            path = extract_path / 'event{:09d}-{}.csv'.format(event_id, name)
            part.to_csv(path, index=False, float_format='%6g')

def extract_nparticles(hits, cells, particles, truth, nparticles, pt_min, rng):
    """Extract the event data for a random subset of particles.
    """
    pt = np.hypot(particles['px'], particles['py'])
    particles_sub = particles[pt_min < pt].sample(n=nparticles, random_state=rng)
    pids = particles_sub['particle_id']
    truth_sub = truth[truth['particle_id'].isin(pids)]
    hids = truth_sub['hit_id']
    cells_sub = cells[cells['hit_id'].isin(hids)]
    hits_sub = hits[hits['hit_id'].isin(hids)]
    return hits_sub, cells_sub, particles_sub, truth_sub

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(description='extract a subset of the dataset')
    parser.add_argument('--random-seed', type=int, default=31415)
    parser.add_argument('--nevents', type=int)
    parser.add_argument('--nparticles', type=int, default=5)
    parser.add_argument('--pt-min', type=float, default=1.0)
    parser.add_argument('dataset_path', type=pathlib.Path)
    parser.add_argument('extract_path', type=pathlib.Path)
    args = parser.parse_args()

    main(**vars(args))
