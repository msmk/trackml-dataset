#!/usr/bin/env python
#
# create a randomized submission from the truth

from __future__ import print_function

import pandas as pd
import trackml.randomize as trnd
from trackml.dataset import load_dataset

def main(dataset_path, submission_file, random_seed, method, param):
    trnd.set_seed(random_seed)

    first_event = True
    for event_id, truth in load_dataset(dataset_path, parts=['truth']):
        print('processing event {}'.format(event_id))

        if method == 'random':
            submission = trnd.random_solution(truth, len(particles))
        elif method == 'drop_hits':
            submission = trnd.drop_hits(truth, param)
        elif method == 'shuffle_hits':
            submission = trnd.shuffle_hits(truth, param)
        else:
            raise Exception('Unknown randomization method \'{}\''.format(method))

        submission['event_id'] = event_id
        kw = {'index': False, 'columns': ['event_id', 'hit_id', 'track_id']}
        if first_event:
            submission.to_csv(submission_file, mode='w', **kw)
            first_event = False
        else:
            submission.to_csv(submission_file, mode='a', header=False, **kw)

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(description='generate randomized submissions')
    parser.add_argument('--random-seed', type=int, default=31415)
    parser.add_argument('--method', default='shuffle_hits')
    parser.add_argument('--param', type=float, default=0.05)
    parser.add_argument('dataset_path')
    parser.add_argument('submission_file')
    args = parser.parse_args()

    main(**vars(args))
