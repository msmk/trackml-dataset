Seattle Trackml Hackathon
=========================

WARNING : this directory hold all files used for the Seattle hackathon. To be kept as archive, while the full trackml challenge is being put in place.


current secret url : https://competitions.codalab.org/competitions/18633?secret_key=a10f3d6b-8887-4c93-ab82-2ea93a2f51f7

old secret url : https://competitions.codalab.org/competitions/18577?secret_key=1604b675-5ff2-4367-b69d-818abc897655

## TODO

- Add Administrators

### Data

- Some sample_submbission to test the leaderboard
	- One from each starting kit

### Code

- The starting kits
- Validate submission (check for formating error)

### Documents

- logo.png
- Fill overview.html with general information about the hackaton
- Fill evaluation.html with information about the scoring
- Fill terms_and_conditions.html with legal terms and conditions
