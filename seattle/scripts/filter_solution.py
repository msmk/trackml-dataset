from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

import os

import numpy as np
import pandas as pd

import glob


def _renumber(ids, random_state=41673):
    """
    Replace all unique ids with randomized small integers.
    """
    rs = np.random.RandomState(seed=random_state)
    unique_ids, inverse_indices = np.unique(ids, return_inverse=True)
    numbers = np.arange(0, len(unique_ids), dtype=unique_ids.dtype)
    rs.shuffle(numbers)
    return numbers[inverse_indices]


def filter_data(data, data_path):
    file_list = glob.glob(os.path.join(data_path, 'event*-*'))
    name_list = set(os.path.basename(file).split('-', 1)[0] for file in file_list)
    name_list = sorted(name_list)
    event_id_list = [int(name[5:]) for name in name_list]
    # https://stackoverflow.com/questions/17071871/select-rows-from-a-dataframe-based-on-values-in-a-column-in-pandas
    data = data.loc[data['event_id'].isin(event_id_list)]
    return data


def filter_solution(solution_path, data_path):
    solution = pd.read_csv(solution_path)
    solution = filter_data(solution, data_path)
    # Make sure the ids are small integers because it breaks the scoring program
    solution['particle_id'] = _renumber(solution['particle_id'])
    solution.to_csv(os.path.join(data_path, "solution.csv"), index=False)


def main(training_solution_path, validation_solution_path, training_dir, validation_dir):
    filter_solution(training_solution_path, training_dir)
    filter_solution(validation_solution_path, validation_dir)


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(description='Filter out events from solution not present in directory.')
    parser.add_argument('training_solution_path')
    parser.add_argument('validation_solution_path')
    parser.add_argument('training_dir')
    parser.add_argument('validation_dir')
    args = parser.parse_args()

    main(**vars(args))
