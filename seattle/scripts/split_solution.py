from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

import os

import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split


def _renumber(ids, random_state=None):
    """
    Replace all unique ids with randomized small integers.
    """
    rs = np.random.RandomState(seed=random_state)
    unique_ids, inverse_indices = np.unique(ids, return_inverse=True)
    numbers = np.arange(0, len(unique_ids), dtype=unique_ids.dtype)
    rs.shuffle(numbers)
    return numbers[inverse_indices]


def load_solution(solution_path):
    solution_dtypes = {
                      'event_id': 'i4',
                      'hit_id': 'i4',
                      'particle_id': 'i8',
                      'weight': 'f4', }
    solution = pd.read_csv(solution_path, header=0, dtype=solution_dtypes)
    return solution


def split_train_test(data, test_size=0.2, random_state=None):
    event_ids = np.unique(data['event_id'])
    train_ids, test_ids = train_test_split(event_ids, test_size=test_size, random_state=random_state)
    # https://stackoverflow.com/questions/17071871/select-rows-from-a-dataframe-based-on-values-in-a-column-in-pandas
    train_data = data.loc[data['event_id'].isin(train_ids)]
    test_data = data.loc[data['event_id'].isin(test_ids)]
    return train_data, test_data


def main(solution_file, random_seed=42, private_size=0.5):
    print("loading solution ... ", end='')
    solution = load_solution(solution_file)
    print("Done")

    # Make sure the ids are small integers because it breaks the scoring program
    solution['particle_id'] = _renumber(solution['particle_id'], random_state=random_seed)

    public_solution, private_solution = split_train_test(solution, test_size=private_size, random_state=random_seed)

    path, ext = os.path.splitext(solution_file)
    public_solution_path = "".join( (path, '_public', ext) )
    private_solution_path = "".join( (path, '_private', ext) )

    print("writing", public_solution_path, " ... ", end='')
    public_solution.to_csv(public_solution_path, index=False)
    print("Done")

    print("writing", private_solution_path, " ... ", end='')
    private_solution.to_csv(private_solution_path, index=False)
    print("Done")


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(description='Split solution file into public and private files.')
    parser.add_argument('--random-seed', type=int, default=42)
    parser.add_argument('--private-size', type=float, default=0.5)
    parser.add_argument('solution_file')
    args = parser.parse_args()

    main(**vars(args))
