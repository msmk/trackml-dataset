#!/usr/bin/env python
import sys
import os
import os.path

from score import compute_score

input_dir = sys.argv[1]
output_dir = sys.argv[2]

submit_dir = os.path.join(input_dir, 'res')
truth_dir = os.path.join(input_dir, 'ref')

if not os.path.isdir(submit_dir):
    print(submit_dir, "doesn't exist")

if os.path.isdir(submit_dir) and os.path.isdir(truth_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # truth_file = os.path.join(truth_dir, "truth.csv")
    truth_file = os.path.join(truth_dir, "truth.csv")
    submission_file = os.path.join(submit_dir, "submission.csv")

    mean_scores, std_scores, n_purity_reco, n_purity_truth = compute_score(submission_file, truth_file)

    output_filename = os.path.join(output_dir, 'scores.txt')
    output_file = open(output_filename, 'w')

    output_file.write("score:{}\n".format(mean_scores))
    output_file.write("std:{}\n".format(std_scores))
    output_file.write("purity_reco:{}\n".format(n_purity_reco))
    output_file.write("purity_truth:{}\n".format(n_purity_truth))

    output_file.close()
