Dataset
=======

This directory holds a few event training file (in subdirectory dataset), =detector.csv= with detector description, 
and a few file derived from the dataset.
Note : TrackML participants will get these dataset from the challenge platform
